use std::pin::Pin;

use async_std::{
    fs::{read_dir, read_to_string, DirEntry, File},
    io::WriteExt,
    path::PathBuf,
};
use chrono::{NaiveDate, Utc};
use futures::{stream::TryStreamExt, Stream, StreamExt};
use log::warn;
use serde::{Deserialize, Serialize};
use serenity::model::id::{ChannelId, GuildId, RoleId};

use crate::{calendar, commands::Frequency, error::Error};

#[cfg(test)]
lazy_static::lazy_static! {
    static ref TEMPDIR: tempdir::TempDir = tempdir::TempDir::new("babel").expect("Failed to create temporary directory for testing");
    static ref BASE_DIR: PathBuf = TEMPDIR.path().into();
}

#[cfg(not(test))]
lazy_static::lazy_static! {
    static ref BASE_DIR: PathBuf = "/opt/babel/".into();
}

#[derive(PartialEq, Eq, Debug, Clone, Serialize, Deserialize)]
pub struct ServerData {
    guild: GuildId,
    channel: ChannelId,
    last_date: usize,
    last_tick: NaiveDate,
    role: Option<RoleId>,
    paused: bool,
    frequency: Frequency,
    tick_length: usize,
}

impl ServerData {
    pub fn new(guild: GuildId, channel: ChannelId) -> Self {
        Self {
            guild,
            channel,
            last_date: Default::default(),
            last_tick: Utc::now().date_naive(),
            role: None,
            paused: false,
            frequency: Frequency::default(),
            tick_length: 2,
        }
    }

    fn get_path(&self) -> PathBuf {
        BASE_DIR.join(self.guild.0.to_string())
    }

    pub async fn stream(
    ) -> Result<Pin<Box<dyn Stream<Item = Result<ServerData, Error>> + std::marker::Send>>, Error>
    {
        Ok(read_dir(&*BASE_DIR)
            .await? // Stream<DirEntry>
            .map_err(|e| e.into())
            .try_filter_map(|entry: DirEntry| async move { Ok(Some(entry.path())) }) // Stream<PathBuf>
            .try_filter_map(|path| async move {
                Ok(path
                    .file_name()
                    .and_then(|s| s.to_str())
                    .and_then(|s| s.parse::<u64>().ok())
                    .map(GuildId))
            }) // Stream<Result<GuildId>>
            .and_then(Self::retrieve)
            .boxed())
    }

    /// Tick forward `count` days and set today as the last_tick.
    pub fn tick(mut self, count: usize) -> Self {
        self.last_tick = Utc::now().date_naive();
        self.last_date += count;
        self
    }

    pub fn tick_length(&self) -> usize {
        self.tick_length
    }

    pub fn set_tick_length(mut self, tick_length: usize) -> Self {
        self.tick_length = tick_length;
        self
    }

    pub fn should_tick_at(&self, day: NaiveDate) -> bool {
        self.frequency.duration() <= day - self.last_tick
    }

    pub fn set_paused(mut self, paused: bool) -> Self {
        self.paused = paused;
        self
    }

    pub fn is_paused(&self) -> bool {
        self.paused
    }

    pub fn get_date(&self) -> calendar::Date {
        calendar::Date::new(self.last_date)
    }

    pub fn get_channel(&self) -> ChannelId {
        self.channel
    }

    pub fn set_channel(mut self, channel: ChannelId) -> Self {
        self.channel = channel;
        self
    }

    pub fn set_date(mut self, date: usize) -> Self {
        self.last_date = date;
        self
    }

    pub fn set_role(mut self, role: RoleId) -> Self {
        self.role = Some(role);
        self
    }

    pub fn allowed_role(self) -> Option<RoleId> {
        self.role
    }

    pub fn set_frequency(mut self, frequency: Frequency) -> Self {
        self.frequency = frequency;
        self
    }

    pub async fn store(&self) -> Result<(), Error> {
        // `create` will truncate the file if it already exists.
        let mut file = File::create(self.get_path()).await?;

        file.write_all(serde_json::to_string(self)?.as_bytes())
            .await?;

        file.sync_all().await?;

        Ok(())
    }

    pub async fn retrieve(guild: GuildId) -> Result<Self, Error> {
        let path = BASE_DIR.join(guild.0.to_string());
        if path.exists().await {
            let contents = read_to_string(path).await?;
            let parsed: Self = serde_json::from_str(&contents)?;
            assert_eq!(guild, parsed.guild);
            Ok(parsed)
        } else {
            warn!("Failed to retrieve guild {}", guild);
            Err(Error::NoData)
        }
    }
}

#[cfg(test)]
mod test {
    use chrono::{Duration, Datelike as _};

    use super::*;

    async fn create_store_data(guild_id: u64) -> ServerData {
        let data = ServerData::new(GuildId(guild_id), ChannelId(guild_id + 1));
        assert!(dbg!(data.clone().store().await).is_ok());
        data
    }

    async fn check_data(check: ServerData) {
        assert_eq!(
            dbg!(ServerData::retrieve(check.guild).await).unwrap(),
            check
        );
    }

    #[tokio::test]
    async fn test_simple_store() {
        create_store_data(1).await;
    }

    #[tokio::test]
    async fn test_simple_retrieve() {
        let data = create_store_data(1).await;
        check_data(data).await;
    }

    #[tokio::test]
    async fn test_retrieve_multiple_stored() {
        let data1 = create_store_data(1).await;
        let data100 = create_store_data(100).await;
        let data10 = create_store_data(10).await;

        check_data(data10).await;
        check_data(data1).await;
        check_data(data100).await;
    }

    #[test]
    // NB: This test could fail in the unlikely event that it runs over a day boundary in UTC.
    // Keep it as short as possible to reduce this risk.
    fn test_ticked() {
        let tick_size = 3;

        // Create data that last ticked yesterday.
        let original_data = {
            let mut data = ServerData::new(GuildId(0), ChannelId(1));
            data.last_tick = data.last_tick - Duration::days(1);
            data
        };

        let mut modified_data = original_data.clone();

        modified_data.last_tick = modified_data.last_tick - Duration::days(1);
        let mut modified_data = modified_data.tick(tick_size);
        assert_eq!(modified_data.last_date, original_data.last_date + tick_size);
        assert_eq!(
            modified_data.last_tick,
            original_data.last_tick + Duration::days(1)
        );

        // Undo expected changes and ensure nothing else changed.
        modified_data.last_tick = original_data.last_tick;
        modified_data.last_date = original_data.last_date;
        assert_eq!(modified_data, original_data);
    }

    #[test]
    fn test_tick_at() {
        let today = Utc::now().date_naive();
        let tomorrow = today + Duration::days(1);
        let yesterday = today - Duration::days(1);
        let one_week_ago = today - Duration::days(7);
        assert_eq!(today.weekday(), one_week_ago.weekday());

        // Create data that last ticked yesterday and ticks daily
            let mut data = ServerData::new(GuildId(0), ChannelId(1));
            data.last_tick = yesterday;
            data.frequency = Frequency::Daily;

            assert!(!data.should_tick_at(yesterday), "Already ticked yesterday");
            assert!(data.should_tick_at(today), "Ticked yesterday, daily tick should trigger today");
            assert!(data.should_tick_at(tomorrow), "Ticked yesterday, daily tick should trigger tomorrow");

            // Change the data to tick weekly
            data.frequency = Frequency::Weekly;

            assert!(!data.should_tick_at(yesterday), "Already ticked yesterday");
            assert!(!data.should_tick_at(today), "Ticked yesterday, weekly tick shouldn't trigger today");
            assert!(!data.should_tick_at(tomorrow), "Ticked yesterday, weekly tick shouldn't trigger tomorrow");

            // Change to have last ticked a week ago
            data.last_tick = one_week_ago;

            assert!(!data.should_tick_at(yesterday), "Ticked a week ago, weekly tick shouldn't trigger yesterday");
            assert!(data.should_tick_at(today), "Ticked a week ago, weekly tick should trigger today");
            assert!(data.should_tick_at(tomorrow), "Ticked a week ago, weekly tick should trigger tomorrow");
    }
}
