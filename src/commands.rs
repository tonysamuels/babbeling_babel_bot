use std::{fmt, str::FromStr};

use chrono::Duration;
use log::debug;
use serde::{Deserialize, Serialize};
use serenity::model::id::{ChannelId, RoleId};

use crate::{calendar::Date, error::Error};

#[derive(PartialEq, Eq, Debug, Clone)]
pub enum Command {
    /// Returns a "Pong" message in the channel.
    Ping,
    /// Forces an immediate tick of size `count` for the guild.
    Tick {
        count: usize,
    },
    /// The default count for a tick
    TickLength {
        length: usize,
    },
    /// Sets how often to tick
    Frequency {
        frequency: Frequency,
    },
    /// Sets the channel for the guild.
    Channel {
        id: ChannelId,
    },
    /// Number of days since day 0. Convert to a date and set the server to it.
    Days {
        count: usize,
    },
    /// The date to set the server to.
    Date {
        date: Date,
    },
    /// Convert days to date, to validate value to Days.
    Convert {
        count: usize,
    },
    /// Set a single allowed role.
    Role {
        id: RoleId,
    },
    /// Pause until un-paused
    Pause,
    // Resume paused state
    Resume,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy, Deserialize, Serialize, Default)]
pub enum Frequency {
    #[default]
    Daily,
    Weekly,
}

impl Frequency {
    /// The duration between instances at the given frequency.
    ///
    /// E.g. a frequncy of daily has a duration of 1 day.
    pub fn duration(&self) -> Duration {
        match self {
            Self::Daily => Duration::try_days(1).expect("1 day is a valid duration"),
            Self::Weekly => Duration::try_days(7).expect("7 days is a valid duration"),
        }
    }
}

impl Command {
    /// Requires a guild to run
    pub fn guild_only(&self) -> bool {
        match self {
            Self::Ping | Self::Convert { .. } => false,
            Self::Tick { .. }
            | Self::Frequency { .. }
            | Self::Channel { .. }
            | Self::Days { .. }
            | Self::Date { .. }
            | Self::Role { .. }
            | Self::TickLength { .. }
            | Self::Pause
            | Self::Resume => true,
        }
    }
}

impl fmt::Display for Command {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<prefix>")?;
        match self {
            Command::Ping => write!(f, "ping"),
            Command::Tick { .. } => write!(f, "tick"),
            Command::Channel { .. } => write!(f, "channel"),
            Command::Days { .. } => write!(f, "days"),
            Command::Convert { .. } => write!(f, "convert"),
            Command::Role { .. } => write!(f, "role"),
            Command::Pause => write!(f, "pause"),
            Command::Resume => write!(f, "resume"),
            Command::Date { .. } => write!(f, "date"),
            Command::Frequency { .. } => write!(f, "frequency"),
            Command::TickLength { .. } => write!(f, "tick_length"),
        }
    }
}

impl FromStr for Command {
    type Err = Error;

    // DDth of MMMM, YY
    fn from_str(content: &str) -> Result<Self, Self::Err> {
        // Read the first word
        let mut word_iter = content.split_ascii_whitespace();
        match word_iter.next().map(|s| s.to_lowercase()).as_deref() {
            Some("ping") => parse_ping(word_iter),
            Some("tick") => parse_tick(word_iter),
            Some("channel") => parse_channel(word_iter),
            Some("days") => parse_days(word_iter),
            Some("convert") => parse_conversion(word_iter),
            Some("role") => parse_role(word_iter),
            Some("pause") => parse_pause(word_iter),
            Some("resume") | Some("unpause") => parse_resume(word_iter),
            Some("date") => parse_date(word_iter),
            Some("frequency") => parse_frequency(word_iter),
            Some("tick_length") => parse_tick_length(word_iter),
            _ => {
                debug!("Unknown command: {}", content);
                Err(Error::UnknownCommand)
            }
        }
    }
}

fn parse_frequency<'a, I>(mut word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    let command = match word_iter.next().map(str::to_lowercase).as_deref() {
        Some("daily") => Ok(Command::Frequency {
            frequency: Frequency::Daily,
        }),
        Some("weekly") => Ok(Command::Frequency {
            frequency: Frequency::Weekly,
        }),
        Some(word) => {
            debug!("Unexpected argument in command: {word}");
            Err(Error::UnexpectedArg(Command::Frequency {
                frequency: Frequency::Daily,
            }))
        }
        None => {
            debug!("Missing argument in command: frequency");
            Err(Error::MissingArg(Command::Frequency {
                frequency: Frequency::Daily,
            }))
        }
    }?;

    if let Some(word) = word_iter.next() {
        debug!("Unexpected argument in command: {word}");
        Err(Error::UnexpectedArg(command))
    } else {
        Ok(command)
    }
}

fn parse_pause<'a, I>(word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    parse_no_arg(Command::Pause, word_iter)
}

fn parse_resume<'a, I>(word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    parse_no_arg(Command::Resume, word_iter)
}

fn parse_ping<'a, I>(word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    parse_no_arg(Command::Ping, word_iter)
}

fn parse_no_arg<'a, I>(command: Command, mut word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    if let Some(word) = word_iter.next() {
        debug!("Unexpected argument in command: {}", word);
        Err(Error::UnexpectedArg(command))
    } else {
        Ok(command)
    }
}

fn parse_tick<'a, I>(word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    parse_single_numeric_arg(Command::Tick { count: 0 }, word_iter)
        .map(|count| Command::Tick { count })
}

fn parse_tick_length<'a, I>(word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    parse_single_numeric_arg(Command::TickLength { length: 0 }, word_iter)
        .map(|length| Command::TickLength { length })
}

fn parse_channel<'a, I>(word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    parse_single_numeric_arg(Command::Channel { id: ChannelId(0) }, word_iter)
        .map(|id| Command::Channel { id })
}

fn parse_role<'a, I>(word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    parse_single_numeric_arg(Command::Role { id: RoleId(0) }, word_iter)
        .map(|id| Command::Role { id })
}

fn parse_days<'a, I>(word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    parse_single_numeric_arg(Command::Days { count: 0 }, word_iter)
        .map(|count| Command::Days { count })
}

fn parse_conversion<'a, I>(word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    parse_single_numeric_arg(Command::Convert { count: 0 }, word_iter)
        .map(|count| Command::Convert { count })
}

fn parse_date<'a, I>(word_iter: I) -> Result<Command, Error>
where
    I: Iterator<Item = &'a str>,
{
    // Parse the rest.
    let date = word_iter.collect::<Vec<_>>().join(" ").parse()?;
    Ok(Command::Date { date })
}

fn parse_single_numeric_arg<'a, T, I>(command: Command, mut word_iter: I) -> Result<T, Error>
where
    T: std::str::FromStr,
    I: Iterator<Item = &'a str>,
{
    if let Some(arg) = word_iter.next() {
        if let Ok(arg) = arg.parse() {
            if let Some(word) = word_iter.next() {
                debug!("Unexpected argument in command: {word}");
                Err(Error::UnexpectedArg(command))
            } else {
                Ok(arg)
            }
        } else {
            debug!("Invalid argument in command: {arg}");
            Err(Error::InvalidArg(command))
        }
    } else {
        debug!("Missing argument in command: {command:?}");
        Err(Error::MissingArg(command))
    }
}

#[cfg(test)]
mod test {
    use matches::assert_matches;

    use super::*;

    fn parse_command(s: &str) -> Result<Command, Error> {
        s.parse()
    }

    #[test]
    fn test_no_command() {
        assert_matches!(parse_command(""), Err(Error::UnknownCommand));
        assert_matches!(parse_command("invalid_command"), Err(Error::UnknownCommand));
    }

    #[test]
    fn test_commands_no_args() {
        assert_matches!(parse_command("ping"), Ok(Command::Ping));
        assert_matches!(parse_command("PING"), Ok(Command::Ping));
        assert_matches!(parse_command("Ping"), Ok(Command::Ping));
        assert_matches!(parse_command("pause"), Ok(Command::Pause));
        assert_matches!(parse_command("resume"), Ok(Command::Resume));
        assert_matches!(parse_command("unpause"), Ok(Command::Resume));
        assert_matches!(
            parse_command("tick"),
            Err(Error::MissingArg(Command::Tick { count: _ }))
        );
        assert_matches!(
            parse_command("channel"),
            Err(Error::MissingArg(Command::Channel { id: _ }))
        );
        assert_matches!(
            parse_command("days"),
            Err(Error::MissingArg(Command::Days { count: _ }))
        );
        assert_matches!(
            parse_command("date"),
            Err(Error::MissingArg(Command::Date { date: _ }))
        );
        assert_matches!(
            parse_command("convert"),
            Err(Error::MissingArg(Command::Convert { count: _ }))
        );
        assert_matches!(
            parse_command("role"),
            Err(Error::MissingArg(Command::Role { id: _ }))
        );
    }

    #[test]
    fn test_commands_one_arg() {
        assert_matches!(
            parse_command("ping 1"),
            Err(Error::UnexpectedArg(Command::Ping))
        );
        assert_matches!(
            parse_command("pause 1"),
            Err(Error::UnexpectedArg(Command::Pause))
        );
        assert_matches!(
            parse_command("unpause 1"),
            Err(Error::UnexpectedArg(Command::Resume))
        );
        assert_matches!(
            parse_command("resume 1"),
            Err(Error::UnexpectedArg(Command::Resume))
        );
        assert_matches!(
            parse_command("channel 1"),
            Ok(Command::Channel { id: ChannelId(1) })
        );
        assert_matches!(parse_command("tick 1"), Ok(Command::Tick { count: 1 }));
        assert_matches!(parse_command("days 1"), Ok(Command::Days { count: 1 }));
        assert_matches!(parse_command("date 1"), Err(Error::DateParse(_)));
        assert_matches!(
            parse_command("convert 1"),
            Ok(Command::Convert { count: 1 })
        );
        assert_matches!(parse_command("role 1"), Ok(Command::Role { id: RoleId(1) }));
    }

    #[test]
    fn test_commands_bad_int() {
        assert_matches!(
            parse_command("tick -1"),
            Err(Error::InvalidArg(Command::Tick { count: _ }))
        );
    }

    #[test]
    fn test_commands_two_args() {
        assert_matches!(
            parse_command("ping 1 2"),
            Err(Error::UnexpectedArg(Command::Ping))
        );
        assert_matches!(
            parse_command("tick 1 2"),
            Err(Error::UnexpectedArg(Command::Tick { count: _ }))
        );
        assert_matches!(
            parse_command("channel 1 2"),
            Err(Error::UnexpectedArg(Command::Channel { id: _ }))
        );
        assert_matches!(
            parse_command("days 1 2"),
            Err(Error::UnexpectedArg(Command::Days { count: _ }))
        );
        assert_matches!(parse_command("date 1 2"), Err(Error::DateParse(_)));
        assert_matches!(
            parse_command("convert 1 2"),
            Err(Error::UnexpectedArg(Command::Convert { count: _ }))
        );
        assert_matches!(
            parse_command("role 1 2"),
            Err(Error::UnexpectedArg(Command::Role { id: _ }))
        );
    }
}
